# Devcontainer Kubernetes Setup

Create namespace

```shell
kubectl create ns r-auth
```

Install mongodb operator

```shell
helm repo add mongodb https://mongodb.github.io/helm-charts
helm install community-operator mongodb/community-operator --namespace r-auth
```

Create mongodb ReplicaSet

```shell
kubectl apply -f kubernetes/mongodb.com_v1_mongodbcommunity_cr.yaml --namespace r-auth
```

Install EMQX

```shell
helm repo add emqx https://repos.emqx.io/charts
helm install emqx emqx/emqx --namespace r-auth
```

Install R-Auth

frontend:

```shell
helm upgrade --install r-auth-client charts/frontend --namespace r-auth --set image.tag=latest
```

backend:

```shell
helm upgrade --install r-auth-server charts/backend --namespace r-auth --set image.tag=latest
```

Authorization Code cron:

```shell
helm upgrade --install r-auth-code charts/backend --namespace r-auth --set image.tag=latest \
    --set runTest=false \
    --set service.create=false \
    --set nameOverride=cron \
    --set "args[0]=node" \
    --set "args[1]=dist/backend/cron/auth-code.js"
```

Auth Data cron:

```shell
helm upgrade --install r-auth-data charts/backend --namespace r-auth --set image.tag=latest \
    --set runTest=false \
    --set service.create=false \
    --set nameOverride=cron \
    --set "args[0]=node" \
    --set "args[1]=dist/backend/cron/auth-data.js"
```

JWKS Key Pair cron:

```shell
helm upgrade --install r-auth-jwks charts/backend --namespace r-auth --set image.tag=latest \
    --set runTest=false \
    --set service.create=false \
    --set nameOverride=cron \
    --set "args[0]=node" \
    --set "args[1]=dist/backend/cron/jwks-key-pair.js"
```

Token cron:

```shell
helm upgrade --install r-auth-token charts/backend --namespace r-auth --set image.tag=latest \
    --set runTest=false \
    --set service.create=false \
    --set nameOverride=cron \
    --set "args[0]=node" \
    --set "args[1]=dist/backend/cron/token.js"
```

Unverified Users cron:

```shell
helm upgrade --install r-auth-users charts/backend --namespace r-auth --set image.tag=latest \
    --set runTest=false \
    --set service.create=false \
    --set nameOverride=cron \
    --set "args[0]=node" \
    --set "args[1]=dist/backend/cron/unverified-users.js"
```
