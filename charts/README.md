# Helm Charts for R-Auth

## Prerequisites

- Create Kubernetes Namespace
- Install MongoDB or FerretDB
- Install MQTT or TCP Events Listener (Optional)
- Refer values override for both helm charts for configurability.

In case you do not want to use helm use `helm template` instead of `helm upgrade --install` to generate the kubernetes resources yaml.

### Create namespace

```shell
kubectl create ns r-auth
```

### Install MongoDB

Install MongoDB community operator

```shell
helm repo add mongodb https://mongodb.github.io/helm-charts
helm install community-operator mongodb/community-operator --namespace r-auth
```

Create MongoDB ReplicaSet

```shell
kubectl apply -f https://gitlab.com/castlecraft/rauth/deploy/-/raw/main/kubernetes/mongodb.com_v1_mongodbcommunity_cr.yaml --namespace r-auth
```

### Install MQTT

```shell
helm repo add emqx https://repos.emqx.io/charts
helm install emqx emqx/emqx --namespace r-auth
```

## Backend Chart

This chart serves the ReST API using the default values. The chart can also be used to start separate CRON deployments instead of starting the cron as part of the ReST API deployment. Expose the backend service to serve routes `/api/*` and `/.well-known/*`.

If the app needs to be served on sub-route (`/r-auth/`) specify the paths as `/r-auth/api/*` and `/r-auth/.well-known/*`.

Following commands install backend with separate deployments for cron processes.

**Note:** `env[12].value=0` will set `ENABLE_CRON=0` from `values.yaml`.

Backend:

```shell
helm upgrade --install r-auth-server charts/backend --namespace r-auth --set "env[12].value=0"
```

Authorization Code cron:

```shell
helm upgrade --install r-auth-code charts/backend --namespace r-auth \
    --set runTest=false \
    --set service.create=false \
    --set nameOverride=cron \
    --set "env[12].value=0" \
    --set "args[0]=node" \
    --set "args[1]=dist/backend/cron/auth-code.js"
```

Auth Data cron:

```shell
helm upgrade --install r-auth-data charts/backend --namespace r-auth \
    --set runTest=false \
    --set service.create=false \
    --set nameOverride=cron \
    --set "env[12].value=0" \
    --set "args[0]=node" \
    --set "args[1]=dist/backend/cron/auth-data.js"
```

JWKS Key Pair cron:

```shell
helm upgrade --install r-auth-jwks charts/backend --namespace r-auth \
    --set runTest=false \
    --set service.create=false \
    --set nameOverride=cron \
    --set "env[12].value=0" \
    --set "args[0]=node" \
    --set "args[1]=dist/backend/cron/jwks-key-pair.js"
```

Token cron:

```shell
helm upgrade --install r-auth-token charts/backend --namespace r-auth \
    --set runTest=false \
    --set service.create=false \
    --set nameOverride=cron \
    --set "env[12].value=0" \
    --set "args[0]=node" \
    --set "args[1]=dist/backend/cron/token.js"
```

Unverified Users cron:

```shell
helm upgrade --install r-auth-users charts/backend --namespace r-auth \
    --set runTest=false \
    --set service.create=false \
    --set nameOverride=cron \
    --set "env[12].value=0" \
    --set "args[0]=node" \
    --set "args[1]=dist/backend/cron/unverified-users.js"
```

## Frontend Chart

This chart serves the Angular SPA for login and administration. This chart serves the Angular app on the base href (`/`) specified.

If the app needs to be served on sub-route (`/r-auth/`) specify the path as `/r-auth`.

Install Frontend Chart with following command:

```shell
helm upgrade --install r-auth-client charts/frontend --namespace r-auth --set image.tag=latest
```
