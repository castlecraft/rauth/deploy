# backend

![Version: 0.2.0](https://img.shields.io/badge/Version-0.2.0-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: 1.16.0](https://img.shields.io/badge/AppVersion-1.16.0-informational?style=flat-square)

A R-Auth Backend Helm chart for Kubernetes

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| affinity | object | `{}` | affinity for deployment |
| args | list | `[]` | command override |
| autoscaling.enabled | bool | `false` | create HPA |
| autoscaling.maxReplicas | int | `100` | max replicas in HPA |
| autoscaling.metrics | list | `[]` | additional metrics to trigger HPA |
| autoscaling.minReplicas | int | `1` | min replicas in HPA |
| autoscaling.targetCPUUtilizationPercentage | int | `80` | target cpu utilization percentage to trigger HPA |
| env | list | `[{"name":"NODE_ENV","value":"production"},{"name":"ADMIN_SET_USER_PWD","value":"1"},{"name":"AUTH_MAX_REQUESTS","value":"10"},{"name":"AUTH_WINDOW","value":"60000"},{"name":"COOKIE_MAX_AGE","value":"7.884e+9"},{"name":"DB_HOST","value":"mongo-0.mongo-svc.r-auth.svc.cluster.local:27017,mongo-1.mongo-svc.r-auth.svc.cluster.local:27017,mongo-2.mongo-svc.r-auth.svc.cluster.local:27017"},{"name":"DB_NAME","value":"rauthdb"},{"name":"DB_USER","value":"rauthuser"},{"name":"ENABLE_CORS","value":"1"},{"name":"ENABLE_RATE_LIMIT","value":"0"},{"name":"ENABLE_SWAGGER","value":"1"},{"name":"EVENTS_HEALTHCHECK","value":"0"},{"name":"EVENTS_HOST","value":"emqx"},{"name":"EVENTS_PORT","value":"1883"},{"name":"EVENTS_PROTO","value":"mqtt"},{"name":"EVENTS_USER","value":"rauth"},{"name":"LOG_LEVEL","value":"log"},{"name":"MONGO_OPTIONS","value":"replicaSet=mongo&ssl=false"},{"name":"MONGO_URI_PREFIX","value":"mongodb"},{"name":"PROD_ENV_VALUE","value":"production"},{"name":"SESSION_NAME","value":"r-auth-io"},{"name":"TOKEN_LENGTH","value":"64"},{"name":"TOKEN_VALIDITY","value":"3600"}]` | environment variables override |
| envFrom | list | `[]` | array of configMapRef and secretRef names for environment variables |
| envSecrets | object | `{"DB_PASSWORD":"admin","EVENTS_PASSWORD":"admin","PASSWORD_ENCRYPTION_KEY":"fHSaw7C@Cs0E@%8TV@E%zc0f38SFWkNe","SESSION_SECRET":"changeit"}` | secret environment variables |
| fullnameOverride | string | `""` | override the full name of the chart. |
| hostAliases | list | `[]` | hostAliases override |
| image.pullPolicy | string | `"IfNotPresent"` | override image pull policy |
| image.repository | string | `"registry.gitlab.com/castlecraft/rauth/backend"` | override image repository |
| image.tag | string | `""` | override the image tag whose default is the chart appVersion. |
| imagePullSecrets | list | `[]` | override the image pull secrets. |
| ingress.annotations | object | `{}` | Ingress annotations |
| ingress.className | string | `""` | Ingress class name |
| ingress.enabled | bool | `false` | create Ingress |
| ingress.hosts | list | `[{"host":"chart-example.local","paths":[{"path":"/(api|.well-known)/(.*)","pathType":"ImplementationSpecific"}]}]` | Ingress hosts |
| ingress.hosts[0].paths[0].path | string | `"/(api|.well-known)/(.*)"` | For subroute use "/example/(api\|.well-known)/(.*)" |
| ingress.tls | list | `[]` | Ingress TLS |
| livenessProbe | object | `{"httpGet":{"path":"/api/healthz","port":"http"}}` | Pod liveness probe, override as exec for cron pods |
| nameOverride | string | `""` | override the chart name. |
| nodeSelector | object | `{}` | node selector for deployment |
| podAnnotations | object | `{}` | pod annotations override |
| podLabels | object | `{}` | pod labels override |
| podSecurityContext | object | `{}` | pod security context override |
| readinessProbe | object | `{"httpGet":{"path":"/api/healthz","port":"http"}}` | Pod readiness probe, override as exec for cron pods |
| replicaCount | int | `1` | set replica count |
| resources | object | `{}` | Pod resources |
| runTest | bool | `true` | run test on helm release |
| securityContext | object | `{}` | security context override |
| service.create | bool | `true` | create service |
| service.port | int | `3000` | service port, can be changed through environment variable set in pod |
| service.type | string | `"ClusterIP"` | service type |
| serviceAccount.annotations | object | `{}` | Annotations to add to the service account |
| serviceAccount.automount | bool | `true` | Automatically mount a ServiceAccount's API credentials? |
| serviceAccount.create | bool | `true` | Specifies whether a service account should be created |
| serviceAccount.name | string | `""` | The name of the service account to use. If not set and create is true, a name is generated using the fullname template |
| tolerations | list | `[]` | tolerations for deployment |
| volumeMounts | list | `[]` | Additional volumeMounts on the output Deployment definition. |
| volumes | list | `[]` | Additional volumes on the output Deployment definition. |

