# frontend

![Version: 0.1.1](https://img.shields.io/badge/Version-0.1.1-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: 1.13.0](https://img.shields.io/badge/AppVersion-1.13.0-informational?style=flat-square)

A R-Auth Frontend Helm chart for Kubernetes

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| affinity | object | `{}` | affinity for deployment |
| autoscaling.enabled | bool | `false` | create HPA |
| autoscaling.maxReplicas | int | `100` | max replicas in HPA |
| autoscaling.metrics | list | `[]` | additional metrics to trigger HPA |
| autoscaling.minReplicas | int | `1` | min replicas in HPA |
| autoscaling.targetCPUUtilizationPercentage | int | `80` | target cpu utilization percentage to trigger HPA |
| env | list | `[{"name":"API_HOST","value":"r-auth-server-backend"},{"name":"API_PORT","value":"3000"}]` | environment variables override |
| fullnameOverride | string | `""` | override the full name of the chart. |
| image.pullPolicy | string | `"IfNotPresent"` | override image pull policy |
| image.repository | string | `"registry.gitlab.com/castlecraft/rauth/frontend"` | override image repository |
| image.tag | string | `""` | override the image tag whose default is the chart appVersion. |
| imagePullSecrets | list | `[]` | override the image pull secrets. |
| ingress.annotations | object | `{}` | Ingress annotations |
| ingress.className | string | `""` | Ingress class name |
| ingress.enabled | bool | `false` | create Ingress |
| ingress.hosts | list | `[{"host":"chart-example.local","paths":[{"path":"/","pathType":"Prefix"}]}]` | Ingress hosts |
| ingress.hosts[0].paths[0].path | string | `"/"` | For subroute use "/example". Also set BASE_PATH env var. |
| ingress.tls | list | `[]` | Ingress TLS |
| livenessProbe | object | `{"httpGet":{"path":"/healthz","port":"http"}}` | Pod liveness probe, override as exec for cron pods |
| nameOverride | string | `""` | override the chart name. |
| nodeSelector | object | `{}` | node selector for deployment |
| podAnnotations | object | `{}` | pod annotations override |
| podLabels | object | `{}` | pod labels override |
| podSecurityContext | object | `{}` | pod security context override |
| readinessProbe | object | `{"httpGet":{"path":"/healthz","port":"http"}}` | Pod readiness probe, override as exec for cron pods |
| replicaCount | int | `1` | set replica count |
| resources | object | `{}` | Pod resources |
| runTest | bool | `true` | run test on helm release |
| securityContext | object | `{}` | security context override |
| service.port | int | `8080` | service port |
| service.type | string | `"ClusterIP"` | service type |
| serviceAccount.annotations | object | `{}` | Annotations to add to the service account |
| serviceAccount.automount | bool | `true` | Automatically mount a ServiceAccount's API credentials? |
| serviceAccount.create | bool | `true` | Specifies whether a service account should be created |
| serviceAccount.name | string | `""` | The name of the service account to use. If not set and create is true, a name is generated using the fullname template |
| tolerations | list | `[]` | tolerations for deployment |
| volumeMounts | list | `[]` | Additional volumeMounts on the output Deployment definition. |
| volumes | list | `[]` | Additional volumes on the output Deployment definition. |

