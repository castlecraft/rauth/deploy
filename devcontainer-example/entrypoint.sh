#!/bin/bash

if [ ! -f "${KUBECONFIG}" ]; then
  mkdir -p ${HOME}/.kube
  touch ${KUBECONFIG}
  curl -sSL k3s:8081>"${KUBECONFIG}"
fi

exec "$@"
