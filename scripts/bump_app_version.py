#!/usr/bin/env python3

import argparse
import sys
from subprocess import check_output

NO_CHANGES_DRY_RUN = "No changes will be made as --dry-run was specified."

cli_print = print  # noqa: T002,T202


def main():
    parser = get_parse_args()
    args = parser.parse_args()

    if len(sys.argv) == 1:
        parser.print_help()
        return

    return bump_app_version(args)


def get_parse_args():
    parser = argparse.ArgumentParser(
        description="Bump appVersion in Given Chart.yaml", add_help=True
    )
    parser.add_argument(
        "-d",
        "--dry-run",
        action="store_true",
        help="DO NOT make changes",
    )
    parser.add_argument(
        "file",
        help="Chart.yaml file to bump",
    )
    parser.add_argument(
        "version",
        help="appVersion to set",
    )

    return parser


def bump_app_version(args):
    if not args.file:
        cli_print("Chart.yaml is not specified")
        return

    if not args.version:
        cli_print("appVersion is empty")
        return

    command = ["yq"]

    current_app_version = get_app_version(args)

    if not args.dry_run:
        command.append("--inplace")

    command += [f'.appVersion = "{args.version}"', args.file]

    cli_print(check_output(command).decode("utf-8"))

    new_app_version = get_app_version(args)

    if (
        current_app_version
        and new_app_version
        and current_app_version != new_app_version
    ):
        cli_print(
            f"appVersion updated from {current_app_version} to {new_app_version}"  # noqa: E501
        )  # noqa: E501


def get_app_version(args):
    return (
        check_output(["yq", ".appVersion", args.file]).decode("utf-8").strip()
    )  # noqa: E501


if __name__ == "__main__":
    main()
