import argparse
import re
import sys


def update_env(service, version: str):
    with open("deploy/compose.yaml", "r+") as f:
        content = f.read()
        content = re.sub(
            "{" + rf"{service.upper()}_VERSION:-.*" + "}",
            "{" + f"{service.upper()}_VERSION:-{version}" + "}",
            content,
        )
        f.seek(0)
        f.truncate()
        f.write(content)


def main():
    parser = get_parse_args()
    args = parser.parse_args()

    if len(sys.argv) == 1:
        parser.print_help()
        return

    update_env(args.service, args.version)


def get_parse_args():
    parser = argparse.ArgumentParser(
        description="Bump version in compose yaml", add_help=True
    )
    parser.add_argument(
        "service",
        choices=["frontend", "backend"],
        help="service to bump version: backend or frontend",
    )
    parser.add_argument(
        "version",
        help="version to set",
    )
    return parser


if __name__ == "__main__":
    raise SystemExit(main())
