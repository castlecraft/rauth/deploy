#!/usr/bin/env python3

import argparse
import os
import sys
from subprocess import check_output

cli_print = print  # noqa: T002,T202


def main():
    parser = get_parse_args()
    args = parser.parse_args()

    if len(sys.argv) == 1:
        parser.print_help()
        return

    return release_chart(args)


def get_parse_args():
    parser = argparse.ArgumentParser(
        description="Release in Given helm chart", add_help=True
    )
    parser.add_argument(
        "-p",
        "--push",
        action="store_true",
        help="push to git",
    )
    parser.add_argument(
        "chart",
        help="helm chart directory",
    )
    return parser


def release_chart(args):
    chart_location = args.chart.split("/")
    chart = chart_location[-1] if chart_location else None

    current_version = get_app_version(chart)
    cli_print(
        check_output(
            [
                "helm",
                "package",
                f"{args.chart}",
                "--destination",
                "charts-archive",
            ]
        ).decode("utf-8"),
    )
    cli_print(
        check_output(
            [
                "helm",
                "repo",
                "index",
                "charts-archive",
                "--url",
                "https://helm.r-auth.io",
            ]
        ).decode("utf-8"),
    )

    new_version = get_app_version(chart)

    if current_version != new_version:
        commit_message = f"chore(release): {chart} helm chart {new_version}"
        cli_print(f"Released with commit message:\n\n{commit_message}")
        check_output(["git", "add", "charts*"])
        check_output(["git", "commit", "-m", f'"{commit_message}"'])
        check_output(["git", "pull", "ssh_origin", "main", "--rebase"])
        if args.push:
            check_output(
                [
                    "git",
                    "push",
                    "ssh_origin",
                    "HEAD:main",
                    "-o",
                    "ci.skip",
                ],
            )
    else:
        cli_print(f"Current Version: {current_version}")
        cli_print(f"New Version: {new_version}")
        cli_print("Version unchanged, reverting changes")
        if args.push:
            check_output(["git", "checkout", "charts*"])


def get_app_version(chart):
    index_file = "charts-archive/index.yaml"
    if os.path.isfile(index_file):
        return (
            check_output(
                [
                    "yq",
                    f".entries.{chart}.[0].appVersion",
                    index_file,
                ],
            )
            .decode("utf-8")
            .strip()
        )  # noqa: E501


if __name__ == "__main__":
    main()
